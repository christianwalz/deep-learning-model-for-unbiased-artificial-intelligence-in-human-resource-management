# Deep Learning Model for Unbiased Artificial Intelligence in Human Resource Management

Bachelorthesis Christian Walz: Reutlingen University

## Deutsch

In dieser Arbeit wird ein Deep Learning Modell, in Form eines faltenden neuronalen Netzes (CNN), für die Klassifizierung von Bewerbungsunterlagen erstellt. Dazu werden Bewerbungsunterlagen gefiltert, die wörter mit GloVe (https://nlp.stanford.edu/projects/glove/) vektorisiert und einem CNN zum Training und zur Evaluation übergeben. Diese Arbeit soll eine Basis für den Einsatz von Deep Learning Modellen im HRM schaffen und unvoreingenommene Ergebnisse liefern.

Dieses Projekt beinhaltet alle Skripte und Datensätze, welche für die Umsetzung der Bachelorthesis benutzt wurden. 
Das Projekt ist in 3 Abschnitte aufgeteilt: Datenaufbereitung (Ordner data), Verwendung von vortrainierten Wort-Einbettungs-Vektoren (Ordner GloVe) und die Erstellung und Optimierung des Deep Learning Modells (Ordner model).
Die Datenaufbereitung besteht aus 5 Skripten und den entsprechenden Datensätzen, auf welche die Skripte angewendet werden. Das erste Skript wandelt alle doc-Dateien in docx-Dateien um. Das zweite Skript ließt pdf- und docx-Dateien ein und speichert sie anschließend in einem DataFrame der pandas-Bibliothek ab. Das dritte Skript erzeugt künstliche Daten mit Hilfe von Data Augmentation. Im vierten Skript wird der Datensatz mit einem weiteren Datensatz erweitert. Im letzen Skript werden Stoppwörter aus dem erweiterten Datensatz entfernt und nur die 1000 häufigsten Wörter, welche in den Daten vorkommen, beibehalten. Zusätzlich werden alle Wörter, welche Voreingenommenheiten (Altersgruppe, Geschlecht, Nationalität, Ethnik, Religion) ausdrücken aus den Daten entfernt, sodass diese gar nicht erst ins Training des CNN einbezogen werden. 
Alle dazu verwendeten Datensätze und Dateien finden sich im Ordner data.
Der Ordner GloVe enthält ein Beispiel für die Verwendung von vortrainierten Wort-Einbettungs-Vektoren. Die 1000 häufigsten Wörter werden mit Hilfe von GloVe in 50 dimensionale Vektoren vektorisiert und anschließend in einem 2D-Plot dargestellt. Wieder finden sich alle dazu verwendeten Dateien und Plots im Ordner GloVe.
Die Erstellung des Deep Learning Modells beinhaltet zwei Ansätze. Ein rekurrentes neuronales Netz (RNN) und ein faltendes neuronales Netz (CNN). In den Skripten der Modelle werden die im Ordner data erstellten DataFrames geladen und mit GloVe vektorisiert. Anschließend wird das Modell erstellt, trainiert, validiert und evaluiert. Der Verlauf der Lernvorgänge wird in einem Plot gespeichert. Das CNN ist in zwei Versionen vorhanden: eine nicht optimierte und eine optimierte Version.
Alle Skripte sind bereit zur Ausführung, solange die Ordnerstruktur nicht verändert wird.

## English

In this work, a Deep Learning model, in the form of a convolutional neural network (CNN), is created for the classification of job applications. For this purpose, application documents are filtered, the words are vectorized using GloVe (https://nlp.stanford.edu/projects/glove/) and passed to a CNN for training and evaluation. This work is intended to provide a basis for the use of Deep Learning models in HRM and to provide unbiased results.

This project contains all scripts and data sets that were used for the implementation of the bachelor thesis. 
The project is divided into 3 sections: Data preparation (data folder), use of pre-trained word embedding vectors (GloVe folder) and the creation and optimization of the Deep Learning model (model folder).
The data preparation consists of 5 scripts and the corresponding datasets to which the scripts are applied. The first script converts all doc files into docx files. The second script reads in pdf and docx files and then stores them in a DataFrame of the pandas library. The third script creates artificial data using Data Augmentation. The fourth script augments the data set with another data set. In the last script, stop words are removed from the augmented dataset and only the 1000 most frequent words that occur in the data are kept. In addition, all words that express biases (age group, gender, nationality, ethnicity, religion) are removed from the data so that they are not even included in the training of the CNN. 
All datasets and files used for this can be found in the data folder.
The GloVe folder contains an example of using pre-trained word embedding vectors. The 1000 most common words are vectorized into 50 dimensional vectors using GloVe and then plotted in a 2D plot. Again, all files and plots used for this can be found in the GloVe folder.
The creation of the deep learning model involves two approaches. A recurrent neural network (RNN) and a convolutional neural network (CNN). In the scripts of the models, the DataFrames created in the data folder are loaded and vectorized with GloVe. Then the model is created, trained, validated and evaluated. The run of the learning processes is stored in a plot. The CNN is available in two versions: a non-optimized and an optimized version.
All scripts are ready to run as long as the folder structure is not changed.


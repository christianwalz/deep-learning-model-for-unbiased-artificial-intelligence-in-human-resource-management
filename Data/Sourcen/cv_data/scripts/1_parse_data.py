from glob import glob
from tika import parser
import textract
import re
import os
import numpy as np
import pandas as pd

dir_base = os.path.dirname(__file__)
dir_base = os.path.dirname(dir_base)
dir_base = os.path.dirname(dir_base)
print("Base directory: "+dir_base)

#Liste von allen Word und PDF Dateien
docx_paths = glob(os.path.join(dir_base)+'\\cv_data\\data_cvs_categorized\\**\\*.docx', recursive=True)
pdf_paths = glob(os.path.join(dir_base) +'\\cv_data\\data_cvs_categorized\\**\\*.pdf', recursive=True)

print(len(docx_paths))
print(len(pdf_paths))

#Leere Labelliste und Textliste anlegen
labellist = []
textlist = []

#Listen einlsen mit Ordnername (Label)
#Word Dateien mit textract
for docp in docx_paths:
    if not (os.path.basename(os.path.dirname(docp)) == "Job_Description") and not (os.path.basename(os.path.dirname(docp)) == "Original_Resumes") and not (os.path.basename(os.path.dirname(docp)) == "Random"):
        print(docp)
        currentTxt = textract.process(docp)
        currentTxt = currentTxt.decode("utf-8") 
        #Alle Sonderzeichen eintfernen (nur Groß- und Kleinbuchstaben bleiben)
        currentTxt = re.sub(r"[^a-zA-Z]+", ' ', currentTxt)
        #Anhängen des Textes an die Textliste
        textlist.append(currentTxt)
        #Ordnername in Labellist speichern
        labellist.append(os.path.basename(os.path.dirname(docp)))

#PDF Dateien mit tika
for pdfp in pdf_paths:
    if not (os.path.basename(os.path.dirname(pdfp)) == "Job_Description") and not (os.path.basename(os.path.dirname(pdfp)) == "Original_Resumes") and not (os.path.basename(os.path.dirname(pdfp)) == "Random"):
        print(pdfp)
        raw = parser.from_file(pdfp)
        currentTxt = raw['content']
        #print(currentTxt)
        #Alle Sonderzeichen eintfernen (nur Groß- und Kleinbuchstaben bleiben)
        currentTxt = re.sub(r"[^a-zA-Z]+", ' ', currentTxt)
        #print(currentTxt)
        #Anhängen des Textes in Textliste
        textlist.append(currentTxt)
        #Ordnername in Labellist speichern
        labellist.append(os.path.basename(os.path.dirname(pdfp)))


#print(len(docx_paths))
#print(len(pdf_paths))
#print(len(docx_paths)+len(pdf_paths))
#print(len(textlist))

#Aus Listen numpy Arrays erzeugen
X_train = np.array(textlist)
Y_train = np.array(labellist)

#Dateframe anlegen und mit den Arrays befüllen
df = pd.DataFrame({'Y_train' : Y_train,'X_train': X_train})
print(df)

#Dateframe in json zwischenspeichern
df.to_json(os.path.join(dir_base)+'\\cv_data\\json_dataframes\\data_after_parsing.json', orient='records', lines=True)




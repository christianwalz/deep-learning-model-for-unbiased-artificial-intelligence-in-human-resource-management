from glob import glob
import re
import os
import win32com.client as win32
from win32com.client import constants


dir_base = os.path.dirname(__file__)
dir_base = os.path.dirname(dir_base)
dir_base = os.path.dirname(dir_base)
print("Base directory: "+dir_base)

#Pfad zu unformatierten Daten
paths = glob(os.path.join(dir_base)+'\\cv_data\\data_original\\Resume&Job_Description\\**\\*.doc', recursive=True)
print(paths)
#Funktion anlegen
def save_as_docx(path):
    #Alte Word Datei öffnen
    word = win32.gencache.EnsureDispatch('Word.Application')
    doc = word.Documents.Open(path)
    doc.Activate ()

    #Pfad umbennen mit docx endung
    new_file_abs = os.path.abspath(path)
    new_file_abs = re.sub(r'\.\w+$', '.docx', new_file_abs)

    #Alte Word Datei unter neuem Format und Namen speichern
    word.ActiveDocument.SaveAs(
        new_file_abs, FileFormat=constants.wdFormatXMLDocument
    )
    #Word Datei schließen
    doc.Close(False)

#print(len(paths))
#Über alle Pfäde iterieren
for path in paths:
    print("Pfad = " + path)
    #Funktion aufrufen
    save_as_docx(path)
    #Alte Datei löschen
    os.remove(path)

 
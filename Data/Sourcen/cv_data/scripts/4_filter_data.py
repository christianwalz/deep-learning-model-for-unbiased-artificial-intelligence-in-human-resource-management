import gensim
import pandas as pd
import numpy as np
import re
from collections import Counter
import os

dir_base = os.path.dirname(__file__)
dir_base = os.path.dirname(dir_base)
dir_base = os.path.dirname(dir_base)
print("Base directory: "+dir_base)


#erweiterte Data in Dataframe laden
all_data= pd.read_json(os.path.join(dir_base)+'\\cv_data\\json_dataframes\\data_after_add_indeed.json', orient='records', lines=True)

#Dataframe wieder auteilen in Labels und Text-Strings
textlist = all_data['X_train']
sentimentlist = all_data['Y_train']

#Eigene Stopwörter laden
text_file = open(os.path.join(dir_base)+"\\cv_data\\custom_stopwords.txt", "r")
#Reihe einlesen und in array speichern
custom_stopwords = [line[:-1] for line in text_file]
text_file.close()
#print(custom_stopwords)
#Eigene Stopwörter mit Gensim Stopwörtern vereinen
my_stopwords = gensim.parsing.preprocessing.STOPWORDS.union(custom_stopwords)
#neue Textliste anlegen in die Texte ohne Stopwörter reinkommen
textlist_without_stopwords = []
#Über alle Texte iterieren
for text in textlist:
    #Nochmals alle Sonderzeichen entfernen, da indeed Daten hinzugekommen sind
    text = re.sub(r"[^a-zA-Z]+", ' ', text)
    #Stoppwörter dem aktuellen Text entfernen und in neue Liste abspeichern
    textlist_without_stopwords.append(gensim.parsing.preprocessing.remove_stopwords(text.lower(),my_stopwords))

#neue List erstellen
splittet_textlist= []
#über Lsite ohne Stoppwörter iterieren
for doc in textlist_without_stopwords:
    #einzelne Texte splitten --> array aus einzelnen wörtern wörtern
    words = doc.split()
    #Der Liste hinzufügen
    splittet_textlist.append(words)

#print(splittet_textlist)

#create Dictionary of the 1000 commonst words:
#weitere Liste
flat_list = []
#über einzelne arrays interieren und diese zu einer Lange 1D Liste zusammenfügen
for sublist in splittet_textlist:
    for item in sublist:
        flat_list.append(item)

#Counter von Wörtern --> Anzahl der einzelnen wörter zählen
Counter = Counter(flat_list)

#1000 häufigsten wörtern abspeichern
words=[]
#word liste erstellen aus häugisten 1000 wörtern
for item in Counter.most_common(1000): 
    words.append(item[0])

#häufigsten wörter int Textfile abspeichern
with open(os.path.join(dir_base)+"\\cv_data\\most_common_words.txt", "w") as f:
    for word in words:
        f.write(word)
        f.write('\n')


#Liste kopieren mit Wörtern
keep = words

#textliste iterieren und alle wörter außer die 1000 häufigsten entfernen
#Liste in der alle fertigen Texte abgespeichert werden
finalDocs=[]
for txt in textlist_without_stopwords:
    temp_string = ""
    #über alle Wörter eines Textes iterien
    for word in txt.split():
        #Wenn wort in den häufigsten wörtern ist beibehalten
        if [keep_word for keep_word in keep if word == keep_word]:
            temp_string+=word+" "
    #der Liste hinzufügen
    newDoc = temp_string
    finalDocs.append(newDoc)

print(len(finalDocs))
print(len(sentimentlist))

#Listen zu numpy arrays machen
X_train = np.array(finalDocs)
Y_train = np.array(sentimentlist)

#wieder als Dataframe abspeichern
df = pd.DataFrame({'Y_train' : Y_train,'X_train': X_train})
#print(df)
print(df)
df.to_json(os.path.join(dir_base)+'\\cv_data\\json_dataframes\\data_final.json', orient='records', lines=True)

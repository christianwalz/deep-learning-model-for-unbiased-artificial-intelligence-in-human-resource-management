from nlpaug.augmenter.word import synonym
import pandas as pd
import nlpaug.augmenter.word as naw
import numpy as np
import os
#nltk.download('wordnet')

dir_base = os.path.dirname(__file__)
dir_base = os.path.dirname(dir_base)
dir_base = os.path.dirname(dir_base)
print("Base directory: "+dir_base)

#Dataframe laden von parseData
all_data= pd.read_json(os.path.join(dir_base)+'\\cv_data\\json_dataframes\\data_after_parsing.json', orient='records', lines=True)

#print(all_data)

#daten von labels, die sehr wenig daten besitzen, aus Dataframe auslesen und in listen speichern
audit_list = []
strategy_list = []
inv_rel_list = []
for index, row in all_data.iterrows():
    if row['Y_train'] == "Audit":
        audit_list.append(row['X_train'])

    if row['Y_train'] == "Strategy":
        strategy_list.append(row['X_train'])

    if row['Y_train'] == "Investor Relations":
        inv_rel_list.append(row['X_train'])

#daten aus dataframe entfernen mit diesen labels
index_to_remove = all_data[all_data['Y_train'] == "Audit"].index
#print(index_to_remove)
all_data.drop(index_to_remove , inplace=True)
index_to_remove = all_data[all_data['Y_train'] == "Strategy"].index
#print(index_to_remove)
all_data.drop(index_to_remove , inplace=True)
index_to_remove = all_data[all_data['Y_train'] == "Investor Relations"].index
#print(index_to_remove)
all_data.drop(index_to_remove , inplace=True)

#print(len(audit_list))
#print(len(strategy_list))
#print(len(inv_rel_list))


#Audit List Augmentation:
#maximale länge der sätze ermitteln
max_length = max(len(sentence.split()) for sentence in audit_list)

#anzahl der synonyme festlegen
synonym_count = int(max_length/2)
#print(synonym_count)

#augmentation initialisieren
aug = naw.SynonymAug(aug_src='wordnet', aug_max=synonym_count)

#print(audit_list[0])
#print(aug.augment(audit_list[0], n =1))

#neue sätze erstellen für jedes Element der Liste 
new_audit_list = audit_list
new_audit_list += aug.augment(audit_list[0], n=10)
new_audit_list += aug.augment(audit_list[1], n=10)
new_audit_list += aug.augment(audit_list[2], n=10)
new_audit_list += aug.augment(audit_list[3], n=10)
new_audit_list += aug.augment(audit_list[4], n=10)
new_audit_list += aug.augment(audit_list[5], n=10)
new_audit_list += aug.augment(audit_list[6], n=10)
print(len(new_audit_list))
X_train = np.array(new_audit_list)

#wieder hinzufügen zu all_data
for i in range(len(new_audit_list)):
    all_data = all_data.append({'Y_train' : 'Audit','X_train': new_audit_list[i]}, ignore_index=True)


#Strategy List Augmentation:
max_length = max(len(sentence.split()) for sentence in strategy_list)

synonym_count = int(max_length/2)
#print(synonym_count)

#Augmentation intialisieren
aug = naw.SynonymAug(aug_src='wordnet', aug_max=synonym_count)

#Synonyme für alle Elemente der Liste erstellen
new_strategy_list = strategy_list
new_strategy_list += aug.augment(strategy_list[0], n=4)
new_strategy_list += aug.augment(strategy_list[1], n=4)
new_strategy_list += aug.augment(strategy_list[2], n=4)
new_strategy_list += aug.augment(strategy_list[3], n=4)
new_strategy_list += aug.augment(strategy_list[4], n=4)
new_strategy_list += aug.augment(strategy_list[5], n=4)
new_strategy_list += aug.augment(strategy_list[6], n=4)
new_strategy_list += aug.augment(strategy_list[7], n=4)
new_strategy_list += aug.augment(strategy_list[8], n=4)
new_strategy_list += aug.augment(strategy_list[9], n=4)
new_strategy_list += aug.augment(strategy_list[10], n=4)
new_strategy_list += aug.augment(strategy_list[11], n=4)
new_strategy_list += aug.augment(strategy_list[12], n=4)
new_strategy_list += aug.augment(strategy_list[13], n=4)
new_strategy_list += aug.augment(strategy_list[14], n=4)
new_strategy_list += aug.augment(strategy_list[15], n=4)

print(len(new_strategy_list))
X_train = np.array(new_strategy_list)

#wieder zum Dataframe hinzufügen
for i in range(len(new_strategy_list)):
    all_data = all_data.append({'Y_train' : 'Strategy','X_train': new_strategy_list[i]}, ignore_index=True)

#Investor Relations Augmentation:
max_length = max(len(sentence.split()) for sentence in inv_rel_list)

synonym_count = int(max_length/2)
#print(synonym_count)

#Augmentation initialsieren
aug = naw.SynonymAug(aug_src='wordnet', aug_max=synonym_count)

#für jedes Element der Liste synonyme erstellen
new_inv_rel_list = inv_rel_list
new_inv_rel_list += aug.augment(inv_rel_list[0], n=10)
new_inv_rel_list += aug.augment(inv_rel_list[1], n=10)
new_inv_rel_list += aug.augment(inv_rel_list[2], n=10)
new_inv_rel_list += aug.augment(inv_rel_list[3], n=10)
new_inv_rel_list += aug.augment(inv_rel_list[4], n=10)
new_inv_rel_list += aug.augment(inv_rel_list[5], n=10)

print(len(new_inv_rel_list))
X_train = np.array(new_inv_rel_list)

#zum Dataframe wieder hinzufügen
for i in range(len(new_inv_rel_list)):
    all_data = all_data.append({'Y_train' : 'Investor Relations','X_train': new_inv_rel_list[i]}, ignore_index=True)



print(all_data)

#Dataframe in json zwischenspeichern
all_data.to_json(os.path.join(dir_base)+'\\cv_data\\json_dataframes\\data_after_augmentation.json', orient='records', lines=True)
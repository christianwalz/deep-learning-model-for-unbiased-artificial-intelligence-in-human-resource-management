import csv
import pandas as pd
import os

dir_base = os.path.dirname(__file__)
dir_base = os.path.dirname(dir_base)
dir_base = os.path.dirname(dir_base)
print("Base directory: "+dir_base)

#alten Dataframe laden
df = pd.read_json(os.path.join(dir_base)+'\\cv_data\\json_dataframes\\data_after_augmentation.json', lines=True)

#print(df)

#Excel mit indeed Daten öffnen
with open(os.path.join(dir_base)+'\\cv_data\\data_indeed\\resume_samples.csv', newline='') as csvfile:
    #Reader initialsieren
    spamreader = csv.reader(csvfile, delimiter=';')

    #try catch für Reader
    try:
        #durch jede Excel Reihe iterieren
        for row in spamreader:
            #Manuelle Selektion der Jobbezeichnungen
            if(row[0].split(":::")[1] == "Systems Administrator" or row[0].split(":::")[1] == "Python Developer" or row[0].split(":::")[1] == "Software Developer" or row[0].split(":::")[1] == "Web Developer" or row[0].split(":::")[1] == "Java Developer" or row[0].split(":::")[1] == "Database Administrator" or row[0].split(":::")[1] == "Sr. Python Developer" or row[0].split(":::")[1] == "Software Engineer" or row[0].split(":::")[1] == "IT Project Manager"):
                #alle Reihen auswählen
                tmp = row[1:-1]
                #Alle Reihen verbinden
                tmp = ' '.join(tmp)
                #DataRow von Dataframe format erzeugen
                new_row = {'Y_train':row[0].split(":::")[1], 'X_train':tmp}
                #An Dataframe anhängen
                df = df.append(new_row, ignore_index=True)
    except:
        print("lel")

#Alle Labels des Dataframes ausgeben
print(df['Y_train'].value_counts(ascending=True))
#Dataframe wieder als json abspeichern
df.to_json(os.path.join(dir_base)+'\\cv_data\\json_dataframes\\data_after_add_indeed.json', orient='records', lines=True) 

#print(df)
#print(tmp)

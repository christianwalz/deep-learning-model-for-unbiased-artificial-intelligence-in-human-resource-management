import tensorflow
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
import keras
from tensorflow.keras.layers import Embedding
from tensorflow.keras.optimizers import Adam
from tensorflow.keras import layers
from tensorflow.keras.layers.experimental.preprocessing import TextVectorization
from tensorflow.keras.callbacks import ModelCheckpoint
import matplotlib.pyplot as plt
import os

dir_base = os.path.dirname(__file__)
dir_base = os.path.dirname(dir_base)
print("Base directory: "+dir_base)

#Funktion um df aus json laden 
def load_and_filter_data():
    all_data= pd.read_json(os.path.join(dir_base)+'\\cv_data\\json_dataframes\\data_final.json', orient='records', lines=True)
    #all_data dataframe ladenzurückgeben
    return all_data


#Dictionary für Labels in Integer --> Model Output
LabelDic = {
    "Administration": 0,
    "Audit":1, 
    "Compliance":2,
    "Corp Accounting, finance":3,
    "Cosec":4,
    "Fund Accounting":5,
    "Investment":6,
    "Investor Relations":7,
    "Legal":8,
    "Operations":9,
    "Personal":10,
    "Sales":11,
    "Strategy":12,
    "Trust":13,
    "IT Project Manager":14,
    "Sr. Python Developer":15,
    "Web Developer":16,
    "Software Developer":17,
    "Python Developer":18,
    "Systems Administrator":19
}


if __name__ == '__main__':
    #df laden (all_data) und liste mit allen 
    all_data = load_and_filter_data()
    #print(all_data)
    
    #daten aus dataframe entfernen mit diesen labels --> AUCH IN INDEED DATA FILE DIREKT MACHEN
    index_to_remove = all_data[all_data['Y_train'] == "Database Administrator"].index
    all_data.drop(index_to_remove , inplace=True)
    index_to_remove = all_data[all_data['Y_train'] == "Java Developer"].index
    all_data.drop(index_to_remove , inplace=True)
    index_to_remove = all_data[all_data['Y_train'] == "Software Engineer"].index
    all_data.drop(index_to_remove , inplace=True)
    #Alle Daten entfernen die keinen Inhalt haben bei X_train
    index_to_remove = all_data[all_data['X_train'] == ""].index
    all_data.drop(index_to_remove , inplace=True)

    #maximale doc-Länge ermitteln, Wichtig für zeropadding bei Tokenizer
    Maximal_Length = max([len(s.split()) for s in all_data['X_train']])

    #Labelname durch LabelDic Integers erstetzen
    for index, row in all_data.iterrows():
        row[0] = LabelDic[row[0]]

    #Daten aus df in train und test Data mit Verhältnis 0.2 teilen
    train, test = train_test_split(all_data, test_size=0.1)
    #train Daten weiter aufsplitten in train und validation Data
    X_train, X_val, y_train, y_val = train_test_split(train, train['Y_train'], test_size=0.2, random_state=42)

    #X_all vorbereiten zu tensor für Vectorizer:
    X_all = all_data['X_train']
    X_all = tensorflow.convert_to_tensor(X_all)

    #Testdaten und Labels vorbereiten:
    #Y_test zu tensoren knovertieren für Verlustfunktion
    y_test = test['Y_train']
    y_test = np.asarray(y_test)
    y_test = tensorflow.convert_to_tensor(y_test, dtype=tensorflow.int64)
    #X_test zu tensor konvertieren für Vektorizer
    X_test = test['X_train']
    X_test = tensorflow.convert_to_tensor(X_test)
    
    #Trainingsdaten und Labels vorbereiten:
    #Y_train zu tensoren knovertieren für Verlustfunktion
    y_train = np.asarray(y_train)
    y_train = tensorflow.convert_to_tensor(y_train, dtype=tensorflow.int64)
    #X_train zu tensor konvertieren für Vektorizer
    X_train = X_train['X_train']
    X_train = tensorflow.convert_to_tensor(X_train)
    
    #Validierungsdaten und Labels vorbereiten
    #Y_val zu tensoren knovertieren für Verlustfunktion
    y_val = np.asarray(y_val)
    y_val = tensorflow.convert_to_tensor(y_val, dtype=tensorflow.int64)
    #X_val zu tensor konvertieren für Vektorizer
    X_val = X_val['X_train']
    X_val = tensorflow.convert_to_tensor(X_val)
    
    #Vektorizer initialiserien mit 1002 (2 wegen undefinded und empty token)
    vectorizer=TextVectorization(max_tokens=1002, output_sequence_length=Maximal_Length)
    #Texte von all_data in Dataset aufteilen
    text_ds = tensorflow.data.Dataset.from_tensor_slices(X_all)
    #Token von Vektorizer auf Dataset von X_all anpassen
    vectorizer.adapt(text_ds)
    # print(vectorizer.get_vocabulary())
    # print(len(vectorizer.get_vocabulary()))

    #vokuabular in dictionary abspeichern mit index (index wird integer wert später)
    voc = vectorizer.get_vocabulary()
    word_index = dict(zip(voc, range(len(voc))))

    #Texte vektorisieren --> Wörtern durch Integer ersetzen
    x_train = vectorizer(X_train)
    x_val = vectorizer(X_val)
    x_test= vectorizer(X_test)

    #GloVe einlesen
    embedding_index = {}
    glove_file = os.path.join(dir_base)+'\\GloVe\\GloVe_data\\glove.6B.100d.txt'
    #Glove öffnen
    with open(glove_file, encoding="utf8") as f:
        #Vectoren für jedes Wort auslesen und in embedding_index speichern (Dictionary)
        for line in f:
            word, coefs=line.split(maxsplit=1)
            coefs = np.fromstring(coefs, "f", sep=" ")
            embedding_index[word] = coefs

    print("%s GloVe Vektoren gefunden!" % len(embedding_index))

    #Parameter für embedding Matrix anlegen
    num_tokens = len(voc) + 2
    embedding_dim = 100
    hits = 0
    misses = 0

    #Embedding matrix mit 0 initialisieren
    embedding_matrix = np.zeros((num_tokens, embedding_dim))
    #durch vokabular dictionary iterieren
    for word, i in word_index.items():
        #temp vektor für Wort aus Vokabular
        embedding_vector = embedding_index.get(word)
        if embedding_vector is not None:
            #wenn Wort in GloVe vorhanden wird er in die entsprechende Reihe der Matrix hinzugefügt 
            embedding_matrix[i] = embedding_vector
            hits += 1
        else:
            #Wenn nicht bleibt Reihe 0 (spielt dann im Deepl learning keine Rolle)
            misses += 1
            #Ausgabe von fehlerhaften Wörtern
            print(word)
    
    #Treffer und Fehler ausgeben
    print("%d Wörter gefunden, %d Wörter waren nicht vorhanden" % (hits, misses))


    #Embedding Layer für Keras erstellen, mit Parametern von Oben und Embedding Matrix --> trainable false, Embedding Vektoren verändern sich nicht
    embedding_layer = Embedding(
        num_tokens,
        embedding_dim,
        embeddings_initializer=keras.initializers.Constant(embedding_matrix),
        trainable=False,
    )

    #Art des Models festlegen (Sequentiell)
    model = keras.Sequential()
    #Input Layer definieren mit Datentyp (hier kommen Tensoren der Trainings, Validierungs und Testdaten (int) rein)
    model.add(keras.Input(shape=(None,), dtype="int64"))
    #Embedding Layer hinzufügen (Macht aus Integern die GloVe Vektoren)
    model.add(embedding_layer)
    #Faltungslayer: 128 Filter mit der Länge 5, EmbeddingLayer filtern
    model.add(layers.Conv1D(128, 5, activation="relu")) 
    #Ergebnis von Filterung reduzieren auf das wichtigste
    model.add(layers.GlobalMaxPooling1D())
    #Dropout Layer gegen overfitting (40% der Knoten werden deaktiviert)
    model.add(layers.Dropout(0.4))
    #Dense Layer mit Gewichten (normale Schicht)
    model.add(layers.Dense(64, activation="relu"))
    #Dropout Layer gegen overfitting (40% der Knoten werden deaktiviert)
    model.add(layers.Dropout(0.4))
    #Letzte Schicht (20 Knoten für 20 Labels)
    model.add(layers.Dense(20, activation="softmax"))

    #Model ausgeben übersicht
    model.summary()
    #Model Ablauf plotten
    tensorflow.keras.utils.plot_model(model, "CNN_optimized_with_shape_info.png", show_shapes=True)

    #optimizer anlegen mit custom Lernrate
    opti = Adam(learning_rate=0.0002)
    #model compilierung festelgen lossfunktion+optimizer + mertic
    model.compile(loss="sparse_categorical_crossentropy", optimizer=opti, metrics=["acc"])

    #Klassengewichte für ausgeglichene anzahl von Labels in batches 
    # class_weight = {
    #      0:1/(list(np.asarray(y_train)).count(0)), 
    #      1:1/(list(np.asarray(y_train)).count(1)), 
    #      2:1/(list(np.asarray(y_train)).count(2)), 
    #      3:1/(list(np.asarray(y_train)).count(3)),
    #      4:1/(list(np.asarray(y_train)).count(4)),
    #      5:1/(list(np.asarray(y_train)).count(5)),
    #      6:1/(list(np.asarray(y_train)).count(6)),
    #      7:1/(list(np.asarray(y_train)).count(7)), 
    #      8:1/(list(np.asarray(y_train)).count(8)), 
    #      9:1/(list(np.asarray(y_train)).count(9)), 
    #      10:1/(list(np.asarray(y_train)).count(10)),
    #      11:1/(list(np.asarray(y_train)).count(11)),
    #      12:1/(list(np.asarray(y_train)).count(12)),
    #      13:1/(list(np.asarray(y_train)).count(13))
    # }

    
    #callback um beste validation accuracy zu speichern
    mcp_save = ModelCheckpoint(os.path.join(dir_base)+'\\model\\model_with_highest_val_acc.hdf5', save_best_only=True, monitor='val_acc', mode='max')
    
    #model trainieren
    history = model.fit(x_train, y_train, batch_size=32, epochs=100, validation_data=(x_val, y_val), verbose=1, callbacks=[mcp_save]) #class_weight=class_weight)
    
    #evaluation des Modells mit testdaten
    loss, accuracy = model.evaluate(x_test, y_test, verbose=1)
    print("\nModel after all Epochs: ")
    print(f'accuracy : {"%3f"%accuracy}')
    print(f'loss : {"%3f"%loss}')

    #Evaluation der Testdaten mit Model mit der höchsten val_acc
    best_val_acc_model = keras.models.load_model(os.path.join(dir_base)+'\\model\\model_with_highest_val_acc.hdf5')
    print("\nBest Val Acc Model: ")
    loss, accuracy = best_val_acc_model.evaluate(x_test, y_test, verbose=1)
    print(f'accuracy : {"%3f"%accuracy}')
    print(f'loss : {"%3f"%loss}')

    #plotten
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('history of learning')
    plt.xlabel('epoch')
    plt.ylabel('accuracy/loss')
    plt.legend(['train acc', 'val acc','train loss','val loss'], loc='upper left')
    plt.savefig(os.path.join(dir_base)+"\\model\\CNN_plot_optimized")
    plt.show()

    
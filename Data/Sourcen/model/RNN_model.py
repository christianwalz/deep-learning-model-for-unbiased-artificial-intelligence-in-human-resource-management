from tensorflow.python.keras.preprocessing import sequence
from keras.preprocessing.text import Tokenizer
import pandas as pd
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
from keras.preprocessing.text import Tokenizer
from tqdm import tqdm
from keras.models import Sequential
from keras.layers import Embedding,LSTM,Dense
from keras.initializers import Constant
import tensorflow
import tqdm
from keras import backend as K
from keras.layers import Dropout
from tensorflow.keras.optimizers import Adam
import matplotlib.pyplot as plt
import os

dir_base = os.path.dirname(__file__)
dir_base = os.path.dirname(dir_base)
print("Base directory: "+dir_base)

#df aus json laden
def load_and_filter_data():
    all_data= pd.read_json(os.path.join(dir_base)+'\\cv_data\\json_dataframes\\data_final_old.json', orient='records', lines=True)
    return all_data

LabelDic = {
    "Administration": 1,
    "Audit":2, 
    "Compliance":3,
    "Corp Accounting, finance":4,
    "Cosec":5,
    "Fund Accounting":6,
    "Investment":7,
    "Investor Relations":8,
    "Legal":9,
    "Operations":10,
    "Personal":11,
    "Sales":12,
    "Strategy":13,
    "Trust":14
}

#Eigenen Tokenizer erstellen --> Datenvorbereitung
class CustomTokenizer:
    max_seq_length = 0
    def __init__(self, train_texts, maximum_length):
        self.train_texts = train_texts
        #Tokenizer erstellen Sequence aus Text mit 1000 individuellen Wörtern
        self.tokenizer = Tokenizer(num_words=1000)
        #Maximale Länge einer Sequence
        self.max_seq_length = maximum_length
        
    def train_tokenize(self):
        # Maximale Länge erhalten
        max_length = len(max(self.train_texts , key=len))
        self.max_length = min(max_length, self.max_seq_length)
    
        # Vokabular anelgen aus Textdaten
        self.tokenizer.fit_on_texts(self.train_texts)
        
    def vectorize_input(self, texts):
        #Daten Vektorisieren
        texts = self.tokenizer.texts_to_sequences(texts)
        #zero padding der Daten
        texts = sequence.pad_sequences(texts, maxlen=self.max_length, truncating='post',padding='post')
        #Vektorisierte und gezeropaddete Texte zurückgeben
        return texts
    
if __name__ == '__main__':
    #Funktion zum Laden der Daten aufrufen
    all_data = load_and_filter_data()
    #print(all_data)
    #maximale doc-Länge ermitteln, Wichtig für zeropadding bei Tokenizer
    maximal_Length = max([len(s.split()) for s in all_data['X_train']])

    #Labels durch Integers von LabelDic ersetzen
    for index, row in all_data.iterrows():
        row[0] = LabelDic[row[0]]

    #Daten aus df in train und test Data mit Verhältnis 0.2 teilen
    train, test = train_test_split(all_data, test_size=0.2)
    #train Daten weiter aufsplitten in train und validation Data
    X_train, X_val, y_train, y_val = train_test_split(train, train['Y_train'], test_size=0.2, random_state=42)

    #Tokenizer initialisieren
    tokenizer = CustomTokenizer(train_texts = all_data['X_train'], maximum_length=maximal_Length)
    #Tokenizer an train vocab anpassen
    tokenizer.train_tokenize()

    #Daten in Token auteilen (Sätze in Wörter und Integer splitten und 0 padden)
    tokenized_train = tokenizer.vectorize_input(X_train['X_train'])
    tokenized_val = tokenizer.vectorize_input(X_val['X_train'])
    tokenized_test = tokenizer.vectorize_input(test['X_train'])

    #Lables und Daten in numpy arrays umwandeln
    tokenized_train = np.asarray(tokenized_train)
    y_train = np.asarray(y_train)
    tokenized_val = np.asarray(tokenized_val)
    y_val = np.asarray(y_val)
    tokenized_test = np.asarray(tokenized_test)
    y_test = np.asarray(test["Y_train"])
    
    #labels an output anpassen One Hot codieren für categorical crossentropy Verlust
    y_train_labels = K.one_hot(y_train, num_classes=14)
    y_val_labels = K.one_hot(y_val, num_classes=14)
    y_test_labels = K.one_hot(y_test, num_classes=14)

    #Pfad zu pretrained Word Embeddings von Glove
    glove_file = os.path.join(dir_base)+'\\GloVe\\GloVe_data\\glove.6B.100d.txt'

    #Länge des Embedding Vektors als Konstante
    EMBEDDING_VECTOR_LENGTH = 100

    #Funktion um Embedding Matrix zu erzeugen
    def construct_embedding_matrix(glove_file, word_index):
        #Dictionary für Matrix
        embedding_dict = {}
        #Glove öffnen
        with open(glove_file, encoding="utf8") as f:
            #Vectoren für jedes Wort auslesen
            for line in f:
                values=line.split()
                #Wort aus File auslesen
                word=values[0]
                #Wenn das Wort im Tokenizer Dic vorhand ist
                if word in word_index.keys():
                    #Vektor auslesen aus Glove File
                    vector = np.asarray(values[1:], 'float32')
                    #Vektor dem Embedding Dictionary für das Keywort hinzufügen
                    embedding_dict[word] = vector

        #Anzahl der Wörter aus Tokenizer Dic bestimmen
        num_words=len(word_index)+1
        
        #Matrix anlegen und mit 0 initialisieren
        embedding_matrix=np.zeros((num_words, EMBEDDING_VECTOR_LENGTH))
        
        #Ladebalken für jedes Wort in Tokenizer Dic (loop)
        for word,i in tqdm.tqdm(word_index.items()):
            if i < num_words:
                #Glove Vektor für aktuelles Wort in Iteration von TokenDic einsetzen
                vect=embedding_dict.get(word, [])
                #Wenn der Vektor existiert diesen in die Matrix für die Stelle der aktuellen Iteration einsetzen
                if len(vect)>0:
                    embedding_matrix[i] = vect[:EMBEDDING_VECTOR_LENGTH] #50 dimensionalen Vektor einsetzen
        return embedding_matrix #fertige Matrix zurückgeben

    #Aufruf von embedding Matrix mit Pfad zu Glove pretrained und Dictionary vom Tokenizer
    embedding_matrix =  construct_embedding_matrix(glove_file, tokenizer.tokenizer.word_index)
    #print(embedding_matrix)
    #print("Matrix_size: Rows=", len(embedding_matrix), "; Columns=", len(embedding_matrix[0]))

    #RNN Model erstellen
    #Funktion die Model erstellt
    def baseline_model():
        #Art des Modells
        model = Sequential()
        #Embedding Layer erstellen
        embedding=Embedding(len(tokenizer.tokenizer.word_index)+1, 
                    EMBEDDING_VECTOR_LENGTH, 
                    embeddings_initializer=Constant(embedding_matrix), 
                    input_length=maximal_Length,
                    trainable=False) 
        #Embedding Schicht dem Modell hinzufügen
        model.add(embedding)
        #Dropout Schicht hinzufügen (60% der Knoten deaktiviert)
        model.add(Dropout(0.6))
        #Long Short Term Memory Layer --> nutzt vorherige Entscheidungen (erinnert sich) mit eigem Droptout
        model.add(LSTM(64, dropout=0.6, recurrent_dropout=0.8)) 
        #Dense Layer mir 32 Knoten als Output und Relu
        model.add(Dense(32, activation='relu'))
        #Dense Layer mir 14 Knoten Output entspricht Anzahl der verschiedenen Labels  
        model.add(Dense(14, activation='softmax'))

	    #Optimierer Adam Anlegen mit Clipvalue: Verhindert gradienten "Explosion"
        optimzer = Adam(clipvalue=0.5) 
        #Optimizer, Loss Function und Einheiten festelgen
        model.compile(optimizer=optimzer, loss='categorical_crossentropy', metrics=['acc'])
        #poisson
        return model

    #model funktion aufrufen
    model = baseline_model()
    #Übersicht des Modells als Bild abspeichern
    tensorflow.keras.utils.plot_model(model, "model1_with_shape_info.png", show_shapes=True)
    #print(model.summary())
    
    #model trainieren
    history = model.fit(tokenized_train, y_train_labels, batch_size=64, epochs=20, validation_data=(tokenized_val,y_val_labels), verbose=1)
    
    #Auswertung
    loss, accuracy = model.evaluate(tokenized_test, y_test_labels, verbose=1)

    print(f'accuracy : {"%3f"%accuracy}')
    print(f'loss : {"%3f"%loss}')

    #Plotten
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('history of learning (model1)')
    plt.xlabel('epoch')
    plt.ylabel('accuracy/loss')
    plt.legend(['train acc', 'val acc','train loss','val loss'], loc='upper left')
    #Fig abspeichern als png
    plt.savefig(os.path.join(dir_base)+"\\model\\RNN_plot")
    plt.show()

import tensorflow
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
import keras
from tensorflow.keras.layers import Embedding
from tensorflow.keras import layers
from tensorflow.keras.layers.experimental.preprocessing import TextVectorization
import matplotlib.pyplot as plt
import os

dir_base = os.path.dirname(__file__)
dir_base = os.path.dirname(dir_base)
print("Base directory: "+dir_base)

#funktion um df aus json zu laden 
def load_and_filter_data():
    all_data= pd.read_json(os.path.join(dir_base)+'\\cv_data\\json_dataframes\\data_final_old.json', orient='records', lines=True)
    #all_data dataframe ladenzurückgeben
    return all_data


#Dictionary für Labels in Integer umwandlung ^= Model Output
LabelDic = {
    "Administration": 0,
    "Audit":1, 
    "Compliance":2,
    "Corp Accounting, finance":3,
    "Cosec":4,
    "Fund Accounting":5,
    "Investment":6,
    "Investor Relations":7,
    "Legal":8,
    "Operations":9,
    "Personal":10,
    "Sales":11,
    "Strategy":12,
    "Trust":13
}

#Main
if __name__ == '__main__':
    #df laden (all_data) 
    all_data = load_and_filter_data()
    #print(all_data)

    #maximale Länge einer gefilterten Bewerbungsunterlage ermitteln
    Maximal_Length = max([len(s.split()) for s in all_data['X_train']])

    #Labelname durch Integer vom LabelDic ersetzten
    for index, row in all_data.iterrows():
        row[0] = LabelDic[row[0]]

    #Daten aus df in train und test Data mit Verhältnis 0.2 teilen
    train, test = train_test_split(all_data, test_size=0.2)
    #train Daten weiter aufsplitten in train und validation Data
    X_train, X_val, y_train, y_val = train_test_split(train, train['Y_train'], test_size=0.2, random_state=42)
    
    #X_all vorbereiten zu tensor für Vectorizer:
    X_all = all_data['X_train']
    X_all = tensorflow.convert_to_tensor(X_all)

    #Testdaten und Labels vorbereiten:
    #Y_test zu tensoren knovertieren für Verlustfunktion
    y_test = test['Y_train']
    y_test = np.asarray(y_test)
    y_test = tensorflow.convert_to_tensor(y_test, dtype=tensorflow.int64)
    #X_test zu tensor konvertieren für Vektorizer
    X_test = test['X_train']
    X_test = tensorflow.convert_to_tensor(X_test)
    
    #Trainingsdaten und Labels vorbereiten:
    #Y_train zu tensoren knovertieren für Verlustfunktion
    y_train = np.asarray(y_train)
    y_train = tensorflow.convert_to_tensor(y_train, dtype=tensorflow.int64)
    #X_train zu tensor konvertieren für Vektorizer
    X_train = X_train['X_train']
    X_train = tensorflow.convert_to_tensor(X_train)
    
    #Validierungsdaten und Labels vorbereiten
    #Y_val zu tensoren knovertieren für Verlustfunktion
    y_val = np.asarray(y_val)
    y_val = tensorflow.convert_to_tensor(y_val, dtype=tensorflow.int64)
    #X_val zu tensor konvertieren für Vektorizer
    X_val = X_val['X_train']
    X_val = tensorflow.convert_to_tensor(X_val)
    
    #Vektorizer initialiserien mit 1002 (2 wegen undefinded und empty token)
    vectorizer=TextVectorization(max_tokens=1002, output_sequence_length=Maximal_Length)
    #Texte von all_data in Dataset aufteilen
    text_ds = tensorflow.data.Dataset.from_tensor_slices(X_all)
    #Token von Vektorizer auf Dataset von X_all anpassen
    vectorizer.adapt(text_ds)
    #print(vectorizer.get_vocabulary())
    #print(len(vectorizer.get_vocabulary()))

    #vokuabular in dictionary abspeichern mit index (index wird integer wert später)
    voc = vectorizer.get_vocabulary()
    word_index = dict(zip(voc, range(len(voc))))

    #Texte vektorisieren --> Wörtern durch Integer ersetzen
    x_train = vectorizer(X_train)
    x_val = vectorizer(X_val)
    x_test= vectorizer(X_test)

    #leere Dictionary erstellen
    embedding_index = {}
    #Pfad zu GloveFile 100 dimensionale Vektoren
    glove_file = os.path.join(dir_base)+'\\GloVe\\GloVe_data\\glove.6B.100d.txt'
    #Glove öffnen
    with open(glove_file, encoding="utf8") as f:
        #Vectoren für jedes Wort auslesen und in embedding_index speichern (Dictionary)
        for line in f:
            word, coefs=line.split(maxsplit=1)
            coefs = np.fromstring(coefs, "f", sep=" ")
            embedding_index[word] = coefs

    #Ausgabe wieviel Vektoren gefunden wurden
    print("%s GloVe Vektoren gefunden!" % len(embedding_index))

    #Parameter für embedding Matrix anlegen
    num_tokens = len(voc) + 2
    embedding_dim = 100
    hits = 0
    misses = 0

    #Embedding matrix mit 0 initialisieren
    embedding_matrix = np.zeros((num_tokens, embedding_dim))
    #durch vokabular dictionary iterieren
    for word, i in word_index.items():
        #temp vektor für Wort aus Vokabular
        embedding_vector = embedding_index.get(word)
        if embedding_vector is not None:
            #wenn Wort in GloVe vorhanden wird er in die entsprechende Reihe der Matrix hinzugefügt 
            embedding_matrix[i] = embedding_vector
            hits += 1
        else:
            #Wenn nicht bleibt Reihe 0 (spielt dann im Deepl learning keine Rolle)
            misses += 1
            #Ausgabe von fehlerhaften Wörtern
            print(word)
    
    #Ausgaben der Treffer und Fehler
    print("%d Wörter gefunden, %d Wörter waren nicht vorhanden" % (hits, misses))


    #Embedding Layer für Keras erstellen, mit Parametern von Oben und Embedding Matrix --> trainable false, Embedding Vektoren verändern sich nicht
    embedding_layer = Embedding(
        num_tokens,
        embedding_dim,
        embeddings_initializer=keras.initializers.Constant(embedding_matrix),
        trainable=False,
    )

    #Art des Models festlegen (Sequentiell)
    model = keras.Sequential()
    #Input Layer definieren mit Datentyp (hier kommen Tensoren der Trainings, Validierungs und Testdaten (int) rein)
    model.add(keras.Input(shape=(None,), dtype="int64"))
    #Embedding Layer hinzufügen (Macht aus Integern die GloVe Vektoren)
    model.add(embedding_layer)
    #Faltungslayer: 128 Filter mit der Länge 5, EmbeddingLayer filtern
    model.add(layers.Conv1D(128, 5, activation="relu")) 
    #Ergebnis von Filterung reduzieren auf das wichtigste
    model.add(layers.MaxPooling1D(5))
    #Faltungslayer: 128 Filter mit der Länge 5, EmbeddingLayer filtern
    model.add(layers.Conv1D(128, 5, activation="relu"))
    #Ergebnis von Filterung reduzieren auf das wichtigste
    model.add(layers.MaxPooling1D(5))
    # #Faltungslayer: 128 Filter mit der Länge 5, EmbeddingLayer filtern
    model.add(layers.Conv1D(128, 5, activation="relu"))
    #Ergebnis von Filterung reduzieren auf das wichtigste
    model.add(layers.GlobalMaxPooling1D())
    #Debse Schicht reduzierung auf 64 Knoten
    model.add(layers.Dense(64, activation="relu"))
    #Letzte Schicht (14 Knoten für 14 Labels)
    model.add(layers.Dense(14, activation="softmax"))
    #Model ausgeben übersicht
    model.summary()

    #Übersicht des Modells als png abspeichern    
    tensorflow.keras.utils.plot_model(model, "CNN_not_optimized_with_shape_info.png", show_shapes=True)

    #Kompilierung des Modells festelgen
    model.compile(loss="sparse_categorical_crossentropy", optimizer="adam", metrics=["acc"])
    
    #Modell trainieren
    history = model.fit(x_train, y_train, batch_size=64, epochs=50, validation_data=(x_val, y_val), verbose=1)   
    
    #evaluation mit testdaten
    loss, accuracy = model.evaluate(x_test, y_test, verbose=1)
    print("\nModel after all Epochs: ")
    print(f'accuracy : {"%3f"%accuracy}')
    print(f'loss : {"%3f"%loss}')

    #plotten
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('history of learning (model2)')
    plt.xlabel('epoch')
    plt.ylabel('accuracy/loss')
    plt.legend(['train acc', 'val acc','train loss','val loss'], loc='upper left')
    #Lernkurve abespeichern
    plt.savefig(os.path.join(dir_base)+"\\model\\CNN_plot_not_optimized")
    plt.show()
import numpy
import os
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
from scipy import spatial

dir_base = os.path.dirname(__file__)
dir_base = os.path.dirname(dir_base)
dir_base = os.path.dirname(dir_base)
print("Base directory: "+dir_base)



#GloVe Dictionary
word2vector = {}

def find_closest_embeddings(embedding):
    return sorted(word2vector.keys(), key=lambda word: spatial.distance.euclidean(word2vector[word], embedding))



if __name__ == '__main__':
    #Create a dictionary with word and corresponding vector
    with open(os.path.join(dir_base+'/GloVe/Glove_data/glove.6B.50d.txt'), encoding="utf8") as file:
        for line in file:
            list_of_values = line.split()    	
            word = list_of_values[0]
            vector_of_word = numpy.asarray(list_of_values[1:], dtype='float32')
            word2vector[word] = vector_of_word


    #1000 häufigsten Wörter laden
    words = open(os.path.join(dir_base+'/GloVe/GloVe_plot/most_common_words_for_GloVe.txt'), "r")
    most_common_words = [line[:-1] for line in words]
    #print(most_common_words)

    #Visualisation
    tsne = TSNE(n_components=2, random_state=0)
    vectors = [word2vector[word] for word in most_common_words]

    #Die 200 häufigsten Vektoren auswählen
    Y = tsne.fit_transform(vectors[:200])
    plt.scatter(Y[:, 0], Y[:, 1])

    for label, x, y in zip(most_common_words, Y[:, 0], Y[:, 1]):
        plt.annotate(label, xy=(x, y), xytext=(0, 0), textcoords="offset points")
    
    #Nahegelegenste GloVe Vektoren finden
    print(find_closest_embeddings(word2vector["programming"])[:5])
    
    plt.title("GloVe Vektoren in 2D")
    plt.xlabel("X")
    plt.ylabel("Y")
    #plot abespeichern als png
    plt.savefig(os.path.join(dir_base+'/GloVe/GloVe_plot/glove_plot_2d.png'))
    plt.show()
